-- This file is part of Photomontage -- skill-demonstration PHP web gallery.
-- 
-- Copyright (C) 2012 Ivaylo Valkov <ivaylo@e-valkov.org>
-- 
-- This program is free software: you can redistribute it and/or
-- modify it under the terms of the GNU Affero General Public License
-- as published by the Free Software Foundation, either version 3 of
-- the License, or (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- Affero General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public
-- License along with this program.  If not, see <http://www.gnu.org/licenses/>.

DROP TABLE IF EXISTS users_roles;

CREATE TABLE users_roles (
       user_id int(10) unsigned NOT NULL,
       role_id int(10) unsigned NOT NULL,
       FOREIGN KEY (user_id) REFERENCES users(id),
       FOREIGN KEY (role_id) REFERENCES roles(id),
       PRIMARY KEY (user_id, role_id)
);

INSERT INTO users_roles VALUES (1, 1);
INSERT INTO users_roles VALUES (2, 2);