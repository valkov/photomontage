-- This file is part of Photomontage -- skill-demonstration PHP web gallery.
-- 
-- Copyright (C) 2012 Ivaylo Valkov <ivaylo@e-valkov.org>
-- 
-- This program is free software: you can redistribute it and/or
-- modify it under the terms of the GNU Affero General Public License
-- as published by the Free Software Foundation, either version 3 of
-- the License, or (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- Affero General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public
-- License along with this program.  If not, see <http://www.gnu.org/licenses/>.

DROP TABLE IF EXISTS users;

CREATE TABLE users (
       id int(10) unsigned NOT NULL AUTO_INCREMENT,
       username VARCHAR(50) NOT NULL,
       password VARCHAR(45) NOT NULL,
       real_name VARCHAR(25) NOT NULL,
       PRIMARY KEY (id),
       UNIQUE KEY (username)
);

INSERT INTO users VALUES (1, 'admin', '8cb2237d0679ca88db6464eac60da96345513964', 'Admin real name');
INSERT INTO users VALUES (2, 'user', '348162101fc6f7e624681b7400b085eeac6df7bd', 'User real name');


