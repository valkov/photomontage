<?php
// This file is part of Photomontage -- skill-demonstration PHP web gallery.
// 
// Copyright (C) 2012 Ivaylo Valkov <ivaylo@e-valkov.org>
// 
// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public
// License along with this program.  If not, see <http://www.gnu.org/licenses/>.

// The directory of the index.php file
define('THIS_DIR', dirname(__FILE__));
define('PHOTOMONTAGE', true);

// Where to look for configuration files
$config_files = array ('/etc/photomontage/config.php',
		       '/usr/local/etc/photomontage/config.php',
		       THIS_DIR."/config.php");
$have_config = 0;

foreach($config_files as $conf) {

  if (file_exists($conf) &&
      (filesize($conf) > 0)) {
    $have_config = $conf;
    break;
  }
}

if (!$have_config) {
  echo "No config files found!";
  exit(1);
}

require_once($have_config);

// Extend include paths
set_include_path(get_include_path().PATH_SEPARATOR.INCLUDE_PATH.'/');
set_include_path(get_include_path().PATH_SEPARATOR.TEMPLATE_DIR.'/');

// Helper function to find the path of our URIs relative to the script
// (index.php) base URI
function uri_for ($path) {

  $server_path = $_SERVER['PATH_INFO'];
  if ($server_path == '/') {
    $server_path = '';
  }

  $uri = str_ireplace($server_path,'',$_SERVER['REQUEST_URI']).$path;
  $uri = str_ireplace('//','/',$uri);
  $uri = preg_replace('/\.php\/$/','.php',$uri);

  return $uri;
}

require_once('photomontage.php');

// Create an instance. The constructor takes care of the rest
$pm = new Photomontage();
unset($pm);
?>