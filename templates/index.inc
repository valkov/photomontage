<?php
// This file is part of Photomontage -- skill-demonstration PHP web gallery.
// 
// Copyright (C) 2012 Ivaylo Valkov <ivaylo@e-valkov.org>
// 
// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public
// License along with this program.  If not, see <http://www.gnu.org/licenses/>.

?>

<h2>Photomontage</h2>
<p>Welcome to Photomontage, a skill-demonstration PHP based web
  gallery.
</p>

<?php if (!$user): ?>
<p>
  To view the <a href="<?php echo uri_for('/gallery') ?>">gallery</a>
  you will have to <a href="<?php echo uri_for('/login')
  ?>">login</a> </p>
<p>
  If you still have no registration, you can 
  <a href="<?php echo uri_for('/register') ?>">sign up here</a>.
</p>
<?php else: ?>
<p>
  This is the front page. The gallery is <a href="<?php echo
  uri_for('/gallery') ?>">here</a>.
</p>
<?php endif; ?>
