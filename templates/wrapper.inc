<?php
// This file is part of Photomontage -- skill-demonstration PHP web gallery.
// 
// Copyright (C) 2012 Ivaylo Valkov <ivaylo@e-valkov.org>
// 
// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public
// License along with this program.  If not, see <http://www.gnu.org/licenses/>.

/* Work-around.  PHP throws an error on plain "<?xml" and "?>" */
/* echo "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" */
?>

<!DOCTYPE html
	  PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
   <title><?php echo 'Photomontage '.$this->request['path']  ?></title>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />

    <meta name="author" content="Ivaylo Valkov - ivaylo@e-valkov.org" />

    <link rel="stylesheet" href="<?php echo uri_for('/css/screen.css') ?>" />
    <link rel="icon" type="image/png" href="<?php echo uri_for('/css/favicon.png') ?>" />
    <link rel="icon" type="image/vnd.microsoft.icon" href="<?php echo uri_for('/css/favicon.ico') ?>" />

</head>
<body>
  <div class="wrap">
    <div class="content">
    <?php include_once (CONTENT) ?>
    <?php if ($user): ?>
    <div class="user-data">
      <p>
      Logged as <?php echo $user['realname'].' ('.$user['username'].')'; ?>
      </p>
      <p>
	<a href="<?php echo uri_for('/logout') ?>">Logout </a>
      </p>
    </div>
    <?php endif; ?>
    </div> <!-- content -->

    <div class="footer">
	<address>
	  <a title="Photomontage, a skill-demonstration PHP based web gallery." href="<?php echo uri_for('/') ?>">Photomontage</a>, 
	  Author: <a href="http://www.e-valkov.org/">Ivaylo Valkov</a>
	  <a href="mailto:Ivaylo Valkov &lt;ivaylo@e-valkov.org&gt;?subject=About Photomontage">&lt;ivaylo@e-valkov.org&gt;</a>
	</address>
    </div> <!-- footer -->

   </div> <!-- wrap -->
</body>
</html>