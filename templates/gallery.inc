<?php
// This file is part of Photomontage -- skill-demonstration PHP web gallery.
// 
// Copyright (C) 2012 Ivaylo Valkov <ivaylo@e-valkov.org>
// 
// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public
// License along with this program.  If not, see <http://www.gnu.org/licenses/>.

?>

<h2>Gallery</h2>
<?php if ($user['role'] == 'admin'): ?>
    <p>
      <a href="<?php echo uri_for('/gallery/image/new') ?>">Upload new image</a>
    </p>
<?php endif; ?>

<?php if (!$images): ?>
<p>
  The gallery is empty. 

  <?php if ($user['role'] == 'admin'): ?> 
  Use the &quot;Upload new image&quot; button to upload some images.
  <?php else: ?>
  Ask the gallery administrator to upload some images. ;)
   <?php endif; ?>
</p>
<?php endif; ?>

<?php foreach($images as $img): 
      $img_title = $img['title'] ? $img['title'] : 'Image '.$img['file'];
?>
<p class="gallery-images">
<a href="<?php echo uri_for('/gallery/image/'.$img['id']) ?>" title="<?php echo $img_title ?>" >
  <img alt="<?php echo $img_title  ?>" 
       src="<?php echo uri_for('/images/'.$img['file']) ?>" />
</a>
</p>
<?php endforeach; ?>

<?php if ($pages['count'] != 1): ?>
<div class="pages">
<?php for ($p=1; $p <= $pages['count'] ; $p++): ?>
<p <?php echo (( $pages['current'] == $p -1 ) ? 'class="current-page"' : '')  ?> >
  <a href="<?php echo uri_for('/gallery/page/'.($p-1))  ?>" title="<?php echo 'Page '.$p ?>" ><?php echo $p ?></a>
</p>
<?php endfor; ?>
</div>
<?php endif; ?>
