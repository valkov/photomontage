<?php
// This file is part of Photomontage -- skill-demonstration PHP web gallery.
// 
// Copyright (C) 2012 Ivaylo Valkov <ivaylo@e-valkov.org>
// 
// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public
// License along with this program.  If not, see <http://www.gnu.org/licenses/>.

?>

<?php 
// Show the form if registration is not successful 
if ($register_success == 0): ?>
<h2>Register new account</h2>
<form id="signup-form" method="post" action="<?php echo uri_for('/register') ?>" >
  <fieldset>
    <p>
      <label for="user">
	Username
      </label>
      <input type="text" name="user" id="user" 
	     <?php 
		if ( $error_user ) {
		echo 'class="input-error"';
		}

		if ( $data_user) {
		echo 'value="'.$data_user.'"';
		}

		?>
	     /> Min. 5
    </p>

    <p>
      <label for="password">
	Password
      </label>
      <input type="password" name="password" id="password" 
	     <?php if ( $error_password ) {
		   echo 'class="input-error"';
		   } 
		   ?>
	     /> Min. 6
    </p>

    <p>
      <label for="password-repeat">
	Repeat password
      </label>
      <input type="password" name="password-repeat" id="password-repeat" 
	     <?php if ( $error_password_repeat ) {
		   echo 'class="input-error"';
		   } 
		   ?>
	     /> Min. 6
    </p>

    <p>
      <label for="real-name">
	Real Name
      </label>
      <input type="text" name="real-name" id="real-name" 
	     <?php if ( $error_real_name ) {
		   echo 'class="input-error"';
		   }

		   if ( $data_real_name) {
		   echo 'value="'.$data_real_name.'"';
		   }
		   ?>

	     /> Min. 3
    </p>

    <p>
      <input type="submit" name="submit" value="Register" />
    </p>
</fieldset>
</form>

<?php if ($error_creating_user): ?>
<p>
  Unable to create account. We are sorry!
</p>
<?php endif; ?>


<?php if ($passwords_differ): ?>
<p>
  The passwords do not match.
</p>
<?php endif; ?>

<?php if ($missing_mandatory_fields): ?>
<p>
  Some mandatory fields were submited empty
</p>
<?php endif; ?>

<?php if ($fields_too_short): ?>
<p>
  Some fields are too short.
</p>
<?php endif; ?>

<?php if ($user_exists): ?>
<p>
  Registration failed. Sorry!
</p>
<?php endif; ?>


<?php if ($password_user_match): ?>
<p>
  The password and the user name cannot be the same. 
</p>
<?php endif; ?>



<?php 
// Show a message for successful registration 
elseif ($register_success): ?>

<p>
  You have registered sucessfully
</p>
<p>
  Please <a href="<?php echo uri_for('/login') ?>">login</a> to view
  the gallery
</p>
<?php endif; ?>
