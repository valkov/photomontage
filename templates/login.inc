<?php
// This file is part of Photomontage -- skill-demonstration PHP web gallery.
// 
// Copyright (C) 2012 Ivaylo Valkov <ivaylo@e-valkov.org>
// 
// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public
// License along with this program.  If not, see <http://www.gnu.org/licenses/>.

?>

<h2>Login</h2>
<form id="login-form" method="post" action="<?php echo uri_for('/login') ?>" >
  <fieldset>
    <p>
      <label for="user">
	Username
      </label>
      <input type="text" name="user" id="user"/>
    </p>

    <p>
      <label for="password">
	Password
      </label>
      <input type="password" name="password" id="password"/>
    </p>

    <p>
      <input type="submit" name="submit" value="Login" />
    </p>


</fieldset>
</form>
