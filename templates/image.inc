<?php
// This file is part of Photomontage -- skill-demonstration PHP web gallery.
// 
// Copyright (C) 2012 Ivaylo Valkov <ivaylo@e-valkov.org>
// 
// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public
// License along with this program.  If not, see <http://www.gnu.org/licenses/>.

?>

<p class="single-image">
<img alt="<?php echo $img['title']  ?>" 
     src="<?php echo uri_for('/images/'.$img['file']) ?>" />

<?php if ($user['role'] == 'admin'): ?>
<div class="delete-image">
<form id="<?php echo 'delete-image-form-'.$c['id'] ?>" 
      method="post" action="<?php echo uri_for('/gallery/image/'.$img['id']) ?>" >
<p>
  <input type="submit" name="delete-image-submit" value="Delete image" />
</p>
</form>
</div>
<?php endif; ?>

</p>

<h2>
  Title: <?php echo $img['title']; ?>
</h2>

<p>
  Return to <a href="<?php echo uri_for('/gallery') ?>">gallery</a>.
</p>

<?php if ($comments[0]['id']): ?>
<h3> Comments</h3>
<?php foreach($comments as $c): ?>
<div class="comment" >
<p id="<?php echo 'comment-'.$c['id'] ?>" >
  <?php echo $c['real_name']." (".$c['username'] ."): ".$c['text'] ?>
</p>
<?php if ($user['role'] == 'admin'): ?>
<form id="<?php echo 'delete-comment-form-'.$c['id'] ?>" 
      method="post" action="<?php echo uri_for('/gallery/image/'.$img['id'].'/comment/'.$c['id']) ?>" >
<p>
  <input type="submit" name="delete-comment-submit" value="Delete comment" />
</p>
</form>
<?php endif; ?>
</div>
<?php endforeach; ?>
<?php else: ?>
<p> No comments yet.</p>
<?php endif; ?>


<form id="new-comment-form" method="post" action="<?php echo uri_for('/gallery/image/'.$img['id'].'/comment') ?>" >
  <fieldset>
    <legend>
      Post a comment
    </legend>
    <p>
      <label for="comment-text">
	Type your comment in the text box:
      </label>
      <textarea name="comment-text" id="comment-text" rows="5" cols="45" 
		<?php
		   if ($error_comment_text) {
		   echo 'class="input-error"';
		   }
		   ?>

		></textarea>
    </p>

    <p>
      <input type="submit" name="sumbit" id="sumbit" value="Post comment" />
    </p>
  </fieldset>
</form>

<?php if ($error_comment_text): ?>
<p>
  The comment cannot be empty.
</p>
<?php endif; ?>
