<?php
// This file is part of Photomontage -- skill-demonstration PHP web gallery.
// 
// Copyright (C) 2012 Ivaylo Valkov <ivaylo@e-valkov.org>
// 
// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public
// License along with this program.  If not, see <http://www.gnu.org/licenses/>.

?>

<h2>Add image to the gallery</h2>
<form id="new-image-form" method="post"  enctype="multipart/form-data"
      action="<?php echo uri_for('/gallery/image/new') ?>" >
  <fieldset>
    <p>
      <label for="image-file">
	Select image to upload to gallery
      </label>
      <!-- Max file size - 1MB -->
      <input type="hidden" id="max-file-size" name="MAX_FILE_SIZE" value="1048576" />
      <input id="image-file" type="file" name="image-file" 
	     <?php
		if ($error_image_file) {
		  echo 'class="input-error"';
		}
	     ?>
	     /> (1MB)
    </p>

    <p>
      <label for="image-title">
	Image title
      </label>
      <input type="text" name="image-title" id="image-title" />
    </p>

    <p>
      <input type="submit" name="sumbit" id="sumbit" value="Upload" />
    </p>
  </fieldset>
</form>

<?php if($upload_success): ?>
<p>
  The file was uploaded successfully.
</p>
<?php endif; ?>


<?php if($error_file_size): ?>
<p>
  The uploaded file is bigger than 1MB.
</p>
<?php endif; ?>

<?php if($error_file_missing): ?>
<p>
  Mandatory fields were submitted empty.
</p>
<?php endif; ?>

<?php if($error_file_type): ?>
<p>
  The file type is not supported. Allowed image files are JPEG, PNG and GIF.
</p>
<?php endif; ?>

<?php if ($error_file_not_uploaded): ?>
<p>
  The file upload failed. Sorry!
</p>
<?php endif; ?>

<p>
  Return to <a href="<?php echo uri_for('/gallery') ?>">gallery</a>.
</p>

