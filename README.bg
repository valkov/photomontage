= Какво е това? = 

Написах този проект през 2012 по време на интервю за работа за младши
програмист на PHP. Това беше първата ми програма на PHP. Отне ми около
седмица след работа (~ 20 часа).

Вероятно програмата е неизползваема в реална обстановка. Целта ѝ е
единствено за демонстрация. Можете да я използвате както прецените,
стига да спазвате лицензното споразумение – Affero GNU GPL версия 3
или по-нова.

Изискванията към проекта бяха:
  * да се създаде уеб галерия 
  * да има два вида потребители  – администратор и обикновен потребител
  * обикновеният потребител да може да се регистрира, да влиза в
    системата, да разглежда снимки и да ги коментира
  * администраторът да може да качва снимки, както ида трие снимки и коментари
  * да не се ползват обкръжения (frameworks)
  * да не се ползват системи за шаблони (template systems)
  * да не се ползват външни библиотеки

= Лиценз = 

Тази програма е свободен софтуер публикуван под условията на GNU
Affero General Public License, версия 3 или (по ваша преценка)
по-нова.

= Структура на кода =

Приложението се опитва да следва програмните модели REST [1] и MVC [2]
(Model–View–Controller). Тъй като, изискването беше да не се използват
обкръжения (framework) и система от шаблони (template system) и всичко
да се изгради из основи, се наложи да оформя някакво бегло подобие на
MVC, за да разделя логиката, достъпа до базата данни и изобразяването
на данните. За краткото време на изпълнение, вероятно има някой
друг-пропуск.

[1] https://secure.wikimedia.org/wikipedia/en/wiki/REST
[2] https://secure.wikimedia.org/wikipedia/en/wiki/Model%E2%80%93view%E2%80%93controller

* lib/ – код и логика;
  ** photomontage.php – клас с основния код. Обработва всички HTTP
     заявки. ; Controller
  ** db.php – клас за достъп до базата данни. Обкръжение около
     функциите mysql_x в PHP; Model

* web_root/
   ** index.php – ; Controller
   ** images/ – изображенията на галерията
   ** css/ – стил
      *** scree.css – основен стил
   ** .htaccess – htaccess.sample
  
* templates/ – шаблони съдържащи XHTML кода ; View
  ** wrapper.inc – съдържа цялостния вид на XHTML страницата
  ** register.inc – регистрация на нов потребител
  ** new_image.inc – качване на ново изображение
  ** login.inc – вход в системата
  ** index.inc – посрещаща страница
  ** gallery.inc – галерия
  ** image.inc  – страница за отделно изображение 
  ** 404.inc – неоткрити страници 
 
* sql/ – структура на базата данни
  ** users.sql – данни за потребители
  ** roles.sql – роли (администратор/потребител)
  ** users_roles.sql – връзка м/в потребители и роли
  ** logged.sql – съдържа хеша в бисквитката за разпознаване на
     влезлите потребители.
  ** images.sql – данни за изображения
  ** comments.sql – коментари
  ** comments_users.sql – връзка между коментари и потребители
  ** comments_images.sql – връзка между коментари и изображения

* mysql_dump.db – изнесена база данни от MySQL
* config.php.sample – примерен конфигурационен файл
* htaccess.sample – примерен файл за ограничаване на публичния достъп
  и достъп без index.php – http://localhost/photomontage/

= Инсталация =

1. Файловете в web_root се копират на достъпно място за уеб
сървъра. Например /var/www/photomontage. Това е и мястото, от което
приложението е достъпно.

2. В web_root (и където се копира) трябва да се сложи файл .htaccess
за Apache, за да се ограничи достъпа до подпапките и да улесни достъпа
до приложението. Примерен файл e htaccess.sample. Изисква се включен
модул mod_rewrite на Apache. Във .htaccess файла трябва да се зададе
пътя, който обслужва приложението в клаузата RewriteBase:

RewriteBase /photomontage/

Приложението работи и без това, но трябва да са отваря през index.php:
http://locahost/photomontage/index.php вместо
http://locahost/photomontage/.

3. Останалите файлове трябва да се копират някъде и да са достъпни за
PHP. Например /usr/local/share/photomontage. 

4. Файлът config.php трябва да се копира на едно от следните места:
   * /etc/photomontage/config.php
   * /usr/local/etc/photomontage/config.php
   * в папката където е index.php

Файлът index.php търси config.php на тези места. Ако config.php ще е
на друго място трябва да се редактира index.php. Целта е кодът на
приложението да не се намира в публично достъпна през уеб сървъра
папка (напр. /var/www).

Копирането на config.php при index.php не е добра идея, тъй като
config.php ще съдържа пароли и други данни, а те не бива да са
достъпни навън, въпреки, че файлът е изпълним (няма да се вижда
съдържание) и ще се изпълни само ако е извикан от index.php. Въпреки
това не е добра практика да пазите конфигурационните файлове на
публично достъпни връзки и пътища. След опита ми с PHP мога да
заключа, че програмирането на PHP е лоша практика само по себе си. }:)

5. Създава се база данни.

Структура на базата има в sql/*.sql и mysql_dump.db.

6.  Редактира се config.php.

Папката, където ще се качват изображенията трябва да се намира в
(дефинирана като IMAGES_DIRECTORY) трябва да е с права за писане за
уеб сървъра, php и index.php. Също така, папката трябва да е достъпна
и публично през сървъра. Изображенията се показват чрез URL
images/img_name.ext спрямо index.php или пътя за достъп
(напр. /photomontage): index.php/images/ или /photomonatage/images/,
ако се използва .htaccess и с mod_rewrite.

   
 
This file is part of Photomontage -- skill-demonstration PHP web gallery.

Copyright (C) 2012 Ivaylo Valkov <ivaylo@e-valkov.org>

This program is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License
as published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public
License along with this program.  If not, see <http://www.gnu.org/licenses/>