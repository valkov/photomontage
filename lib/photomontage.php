<?php
// This file is part of Photomontage -- skill-demonstration PHP web gallery.
// 
// Copyright (C) 2012 Ivaylo Valkov <ivaylo@e-valkov.org>
// 
// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public
// License along with this program.  If not, see <http://www.gnu.org/licenses/>.

/* Protect the code from execution if not accessed from index.php
   where PHOTOMONTAGE is defined. */
if (!defined('PHOTOMONTAGE')) {
  exit(1);
}

require_once('db.php');

Class Photomontage
{
  // Request data
  private $request;
  // True if a user is authenticated
  private $userid;

  // The role of an authenticated (admin/user)
  private $user_role;

  // Templates data
  private $templates;

  // Holds variables for the scope of the templates
  private $stash;

  // Where gallery images are stored on disk.
  // Usually under a subdirectory in the web server's root directory.
  private $image_directory;

  // Database object
  private $model;
  
  function __construct() {
    $this->request = array(
			   'uri' => $_SERVER['REQUEST_URI'],
			   'method' => $_SERVER['REQUEST_METHOD'],
			   'path'  => ($_SERVER['PATH_INFO'] ? $_SERVER['PATH_INFO']  : '/'),
			   'params' => $_POST,
			   'cookies' => $_COOKIE);
    $this->userid = -1;
    $this->user_role = -1;

    // Tempaltes
    $this->templates = array (
			'extension' => TEMPLATE_EXTENSION,
			'wrapper' => TEMPLATE_WRAPPER,
			'dir' => TEMPLATE_DIR);

    // Prepare the stash
    $this->stash = array();

    // Images directory
    $this->images_directory = IMAGES_DIRECTORY;
    // How many images per page to show
    $this->images_per_page = IMAGES_PER_PAGE;
  
    /* Start processing requests  */
    $this->process();
  }

  function __destruct() {
    if ($this->model) {
      unset($this->model);
    }
  }

  // Processes browser requests and dispatches them to the
  // approprieate function for the path/action.
  private function process() {

    // Stash / tempaltes variables default values; PHP complains if
    // they are undefined.

    $this->stash['pages'] = 0;
 
    $this->stash['user'] = 0;
    $this->stash['db_error'] = 0;

    // New image
    $this->stash['error_image_file'] = 0;
    $this->stash['error_file_size'] = 0;
    $this->stash['error_file_type'] = 0;
    $this->stash['upload_success'] = 0;
    $this->stash['error_file_missing'] = 0;
    $this->stash['error_file_not_uploaded'] = 0;

    // Register
    $this->stash['passwords_differ'] = 0;
    $this->stash['missing_mandatory_fields'] = 0;
    $this->stash['error_user']  = 0;
    $this->stash['error_password']  = 0;
    $this->stash['error_password_repeat']  = 0;
    $this->stash['register_success'] = 0;
    $this->stash['password_user_match'] = 0;
    $this->stash['fields_too_short'] = 0;
    $this->stash['user_exists'] = 0;
    $this->stash['error_creating_user'] = 0;

    $this->stash['data_user']  = '';
    $this->stash['data_real_name']  = '';

    // Gallery
    $this->stash["error_empty_gallery"] = 0;

    $path = $this->request['path'];

    switch (strtolower($path)) {
    case '/':
      $this->index();
      break;
    case '/register':
    case '/register/':
      $this->register();
      break;
    case "/login/":
    case "/login":
      $this->login();
      break;
    case "/logout/":
    case "/logout":
      $this->logout();
      break;
    case "/gallery/":
    case "/gallery":
    case (preg_match(':/gallery/page(/(\d+)*)*$:i',
		     $path) ? $path : '-1'):
       $this->gallery();
      break;
    case "/gallery/image/new":
      $this->new_image();
      break;
    case (preg_match(':/gallery/image/\d+$:i',
		     $path) ? $path : '-1'):
      $this->image();
      break;
    case  (preg_match(':/gallery/image/\d+/comment(/(\d+)*)*$:i',
		      $path) ? $path : '-1'):
      $this->comment();
      break;
    default:
      header("HTTP/1.1 404 Not Found");
      $this->template = "404";
    }

    $this->render();
  }

  // Handles user registrations
  function register() {

    $this->check_login();
    if ($this->userid > 0) {
      $this->redirect(uri_for('/gallery'));
    }

    if (strtoupper($this->request['method']) == 'POST') {

      $error_on_submit = 0;
      
      $this->stash['data_user']  = $this->request['params']['user'];
      $this->stash['data_real_name']  = $this->request['params']['real-name'];

      $data_length = array ('user' => 5,
			    'password' => 6,
			    'password-repeat' =>6,
			    'real-name' => 3);
      
      // Missing data
      foreach ($data_length as $key => $value) {

	$param = $key;
	$len = $value;

	if(!$this->request['params'][$param]) {

	  $error_on_submit = 1;
	  $this->stash['missing_mandatory_fields'] = 1;
	  $this->stash['error_'.str_replace('-','_',$param)]  = 1;
	}

	// Check for password and user match
	if( $this->request['params'][$param] && 
	    strlen($this->request['params'][$param]) < $len ) {
	  $error_on_submit = 1;
	  $this->stash['error_'.str_replace('-','_',$param)]  = 1;
	  $this->stash['fields_too_short'] = 1;
	}
      }


      // Check for password and user match
      if($this->request['params']['password'] == 
	 $this->request['params']['user']) {

	$this->stash['error_user']  = 1;
	$this->stash['error_password']  = 1;
	$this->stash['password_user_match'] = 1;
	$error_on_submit = 1;
      }
      

      // Check for matching passwords
      if($this->request['params']['password'] != 
	 $this->request['params']['password-repeat']) {

	$this->stash['error_password']  = 1;
	$this->stash['error_password_repeat']  = 1;
	$this->stash['passwords_differ'] = 1;
	$error_on_submit = 1;

      }

      if (!$error_on_submit) {
	// Insert in DB

	if (!$this->model) {
	  $this->db_init();
	}

	if (!$this->model) {
	  $this->stash['db_error'] = 1;
	  $this->render();
	} else {
	  $user = strip_tags($this->request['params']['user']);
	  $pass = sha1($this->request['params']['password']);
	  $real_name = strip_tags($this->request['params']['real-name']);

	  $query = sprintf("SELECT username FROM users WHERE ".
			   "username='%s'",
			   $this->model->escape($user));

	  $res = $this->model->fetch_assoc($this->model->query($query));

	  if ($res["username"]) {
	    $this->stash['user_exists']  = 1;
	    $this->stash['error_user']  = 1;
	  } else {

	    $query = sprintf("SELECT id FROM roles WHERE ".
			     "role_name='%s'",
			     $this->model->escape('user'));

	    $res = $this->model->fetch_assoc($this->model->query($query));

	    if ($res['id']) {
	      $role_id = $res['id'];
	      $query = sprintf("INSERT INTO users VALUES(DEFAULT(id),".
			       "'%s', '%s', '%s')", 
			       $this->model->escape($user), 
			       $this->model->escape($pass),
			       $this->model->escape($real_name));


	      $this->model->query($query);

	      $last_id = $this->model->insert_id();

	      if ($last_id) {
		$this->stash['register_success'] = 1;
		$query = sprintf("INSERT INTO users_roles VALUES('%s','%s')",
				 $this->model->escape($last_id),
				 $this->model->escape($role_id));

		$this->model->query($query);
	      } else {
		$this->stash['error_creating_user'] = 1;
	      }
	    }
	  }
	}
      }
    }

    $this->template = "register";
  }

  // Handles user logout 
  function logout () {
    $this->check_login();
    if ($this->userid < 0) {
      $this->redirect(uri_for('/'));
    }

    if (!$this->model) {
      $this->db_init();
    }

    if (!$this->model) {
      $this->stash['db_error'] = 1;
      $this->render();
    } else {
      $query = sprintf("SELECT * FROM logged WHERE ".
		       "user_id='%s'",
		       $this->model->escape($this->userid));

      $res = $this->model->fetch_assoc($this->model->query($query));

      if ($res["user_id"]) {

	setcookie("photomontage_logged",
		  $res['hash'],
		  (time()-3600)); // Expired login
      }

      $query = sprintf("DELETE FROM logged WHERE user_id='%s'",
		       $this->model->escape($this->userid));

      $this->model->query($query);
      $this->redirect(uri_for('/'));
    }
  }

  // Handles user login
  function login() {
    $this->check_login();

    if ($this->userid > 0) {
      $this->redirect(uri_for('/gallery'));
    }

    if (strtoupper($this->request['method']) == 'POST') {

      if ($this->request['params']['user'] &&
	  $this->request['params']['password']) {

	$user = $this->request['params']['user'];
	$pass = $this->request['params']['password'];

	if (!$this->model) {
	  $this->db_init();
	}

	if (!$this->model) {
	  $this->stash['db_error'] = 1;
	  $this->render();
	} else {
	  $query = sprintf("SELECT users.id,users.username,".
			   "users.real_name,roles.role_name,".
			   "users_roles.role_id ".
			   " FROM users_roles ".
			   " LEFT JOIN users ".
			   " ON users_roles.user_id = users.id ".
			   " LEFT JOIN roles ".
			   " ON users_roles.role_id = roles.id WHERE ".
			   " username='%s' AND password='%s'",
			   $this->model->escape($user),
			   sha1($this->model->escape($pass)));

	  $res = $this->model->fetch_assoc($this->model->query($query));

	  if ($res["id"]) {

	    $this->userid = $res["id"];
	    $this->user_role = $res['role_name'];

	    $sha1 = sha1($this->userid);

	    setcookie("photomontage_logged",
		      $sha1,
		      (time()+3600)); // Expires after one hour

	    $login = sprintf("INSERT INTO logged VALUES('%s', '%s')",
  			     $this->model->escape($this->userid),
  			     $this->model->escape($sha1));
  	    $this->model->query($login);
	    
  	    $this->stash['user'] = array ('username' => $res['username'],
  					  'realname' => $res['realname'],
  					  'role' => $res['role_name']);

  	    $this->redirect(uri_for('/gallery'));
  	  }
  	}
      }
    }

    $this->template = "login";
  }

  // Handles requests fo the root of the website / web service
  function index() {
    $this->check_login();
    $this->template = "index";
  }

  // Handles the gallery view
  function gallery() {
    $this->force_login();

    $pages = 0;
    $image_count = 0;
    $first_in_page = 0;

    $path = strtolower($this->request['path']);
    $matches = 0;
    $page = 0;

    if (preg_match(':/gallery/page/(\d+)$:i', $path, $matches)) {
      $page = $matches[1];
    }

    if (!$this->model) {
      $this->db_init();
    }

    if (!$this->model) {
      $this->stash['db_error'] = 1;
      $this->render();
    }

    $query = sprintf("SELECT COUNT(id) AS images FROM images");

    $res = $this->model->fetch_assoc($this->model->query($query));

    if ($res['images']) {
      $image_count = $res['images'];

      $pages = round($image_count / $this->images_per_page);
      $pages = $pages ? $pages : 1;

      if ($page == 0) {
	$first_in_page = 0;
	$last_in_page = $this->images_per_page;
      } else {
	$last_in_page =  $this->images_per_page * $page;
	$first_in_page =  $this->images_per_page + $page;
	// Decrement with 1 to work-around the exclusion of the lower
	// limit in the SQL query
	$first_in_page --;
      }
    }

    $query = sprintf("SELECT * FROM images LIMIT %d,%d",
		     $this->model->escape(intval($first_in_page)),
		     $this->model->escape($last_in_page));

    $rows = $this->model->query($query);

    if ($this->model->rows_count($rows) == 0) {
      if ($image_count) {
	// No such page
	header("HTTP/1.1 404 Not Found");
	$this->template = "404";
	$this->render();
	exit(1);
      }
      // Must be an empty gallery
      $this->stash["error_empty_gallery"] = 1;
    } else {
      $this->stash['images'] = array ();

      while ($res = $this->model->fetch_assoc($rows)) {
	$img = array(
		    'id' => $res['id'],
		    'file' => basename($res['path']),
		    'title' => $res['title']);
	
	array_push($this->stash['images'], $img);
      }

      $this->stash['pages'] = array('count' =>$pages,
				    'current' =>$page);
    }

    $this->template = "gallery";
  }

  // Handles image uploads
  function new_image() {
    $this->force_login();

    if ($this->user_role != 'admin') {
      $this->redirect(uri_for('/gallery'));
    }

    if (strtoupper($this->request['method']) == 'POST') {
      $file = $_FILES['image-file'];
      $error_on_submit = 0;

      if (!$file || $file['size'] <= 0) {
	$this->stash['error_image_file'] = 1;
	$this->stash['error_file_missing'] = 1;
	$error_on_submit = 1;
      }

      if ($file['size'] > 1048576) {
	$this->stash['error_image_file'] = 1;
	$this->stash['error_file_size'] = 1;
	$error_on_submit = 1;
      }

      // Get mime
      if ($file && $file['size'] > 0) {
	$file_info = finfo_open(FILEINFO_MIME_TYPE);
	$mime = finfo_file($file_info, $file['tmp_name']);

	if( $mime != 'image/jpeg'  &&
	    $mime != 'image/pjpeg' &&
	    $mime != 'image/png'   &&
	    $mime != 'image/gif' ) {

	  $this->stash['error_image_file'] = 1;
	  $this->stash['error_file_type'] = 1;
	  $error_on_submit = 1;
	}
      }

      if ($file['error'] !== UPLOAD_ERR_OK) {
	$this->stash['error_image_file'] = 1;
	$this->stash['error_file_not_upload'] = 1;
	$error_on_submit = 1;
      } else if (!is_uploaded_file($file['tmp_name'])) {
	$this->stash['error_image_file'] = 1;
	$this->stash['error_file_not_uploaded'] = 1;
	$error_on_submit = 1;
      } else {
	$save_path = $this->images_directory.
	  DIRECTORY_SEPARATOR.
	  basename($file['name']);

	$cnt = 0;

	// Make sure filename is unique.
	while (file_exists($save_path) || 
	       $cnt >= 1000 ) {
	  $rand = rand(0,1000);
	  $save_path = str_replace('.','-'.$rand.'.',$save_path);
	  $cnt ++;
	}

	// Protect against potential loop if 1000 files with the same
	// name are uploaded
	if ($cnt >= 1000) {
	  $this->stash['error_image_file'] = 1;
	  $this->stash['error_file_not_uploaded'] = 1;
	  $error_on_submit = 1;
	}

	if (!$error_on_submit) {
	  if (!$this->model) {
	    $this->db_init();
	  }

	  if (!$this->model) {
	    $this->stash['db_error'] = 1;
	    $this->render();
	  } else {
	    $image_title = strip_tags($this->request['params']['image-title']);

	    $query = sprintf("INSERT INTO images ".
			     "VALUES(DEFAULT(id), '%s', '%s')",
			     $this->model->escape($save_path),
			     $this->model->escape($image_title));

	    $this->model->query($query);

	    $last_id = $this->model->insert_id();

	    if ($last_id) {
	      if(move_uploaded_file($file['tmp_name'], $save_path)) {
		$this->stash['upload_success'] = 1;
	      } else {
		$this->stash['error_image_file'] = 1;
		$this->stash['error_file_not_uploaded'] = 1;
	      }

	    } else {
	      $this->stash['error_image_file'] = 1;
	      $this->stash['error_file_not_uploaded'] = 1;
	    }
	  }
	}
      }
    }

    
    $this->template = "new_image";
  }

  // Handles per image views
  function image() {
    $this->force_login();

    $path = strtolower($this->request['path']);
    $matches = 0;

    if (preg_match(':/gallery/image/(\d+)$:i',  $path, $matches)) {
      $image_id = $matches[1];
    }

    if (!$this->model) {
      $this->db_init();
    }

    if (!$this->model) {
      $this->stash['db_error'] = 1;   
      $this->render();
    }

    $query = sprintf("SELECT users.id AS user_id,users.username,".
		     "users.real_name,comments.text AS comment_text,".
		     "comments.id AS comment_id,images.id AS image_id,".
		     "images.path AS image_path,".
		     "images.title AS image_title ".
		     " FROM images ".
		     "LEFT JOIN comments_images ON images.id = ".
		     "comments_images.image_id ".
		     "LEFT JOIN comments_users ON ".
		     " comments_images.comment_id = ".
		     "comments_users.comment_id ".
		     " LEFT JOIN users ON comments_users.user_id = ".
		     " users.id ".
		     "LEFT JOIN comments ON ".
		     " comments_images.comment_id = comments.id WHERE ".
		     " images.id = '%d'",
		     $this->model->escape($image_id));

    $rows = $this->model->query($query);

    if ($this->model->rows_count($rows) == 0) {
      // No such image
      header("HTTP/1.1 404 Not Found");
      $this->template = "404";
      $this->render();
      exit(1);
    }

    $delete_comment_ids = array();

    $this->stash['comments'] = array();
    while ($res = $this->model->fetch_assoc($rows)) {
      if ( !($res['comment_id'] >= 0) ) {
	continue;
      }

      // Collect comment ids for image related comments to be deleted
      if (strtoupper($this->request['method']) == 'POST' &&
	  $this->request['params']['delete-image-submit']) {
	array_push($delete_comment_ids, 
		   sprintf(" '%s' ",
			   $this->model->escape($res['comment_id'])));
      }

      $comment = array(
		       'id' => $res['comment_id'],
		       'text' => $res['comment_text'],
		       'username' => $res['username'],
		       'real_name' => $res['real_name']
		       );
     
      array_push($this->stash['comments'], $comment);
      $this->stash['img'] = array(
				  'id' => $res['image_id'],
				  'file' =>basename($res['image_path']),
				  'title' => $res['image_title']);

    }

      if (strtoupper($this->request['method']) == 'POST' &&
	  $this->request['params']['delete-image-submit']) {
	
	$query = "DELETE FROM comments_images WHERE comment_id = ".
	  join(' OR comment_id = ',$delete_comment_ids);

	$this->model->query($query);

	$query = "DELETE FROM comments_users WHERE comment_id = ".
	  join(' OR comment_id = ',$delete_comment_ids);

	$this->model->query($query);

	$query = "DELETE FROM comments WHERE id = ".
	  join(' OR id = ',$delete_comment_ids);

	$this->model->query($query);

	$query = sprintf("DELETE FROM images WHERE id = '%s'",
			 $this->model->escape($image_id));

	$this->model->query($query);

	$this->redirect(uri_for('/gallery'));
      }

    $this->template = "image";
  }

  function comment() {  
    $this->force_login();

    $path = strtolower($this->request['path']);
    $matches = 0;

    $image_id = null;
    $comment_id = null;

    if (preg_match(':/gallery/image/(\d+)/comment/(\d+):i',
		   $path,$matches)) {
      $comment_id = $matches[2];
      $image_id = $matches[1];
    } else if (preg_match(':/gallery/image/(\d+)/comment(/)*$:i',
			  $path,$matches)) {
      $image_id = $matches[1];
    }
    
    // Nothing to show this function handles only POST requests to add
    // and delete comments. Redirect to image page with #comment-x if
    // available
    if (strtoupper($this->request['method']) != 'POST') {
      if (!$image_id) {
	$this->redirect(uri_for('/gallery'));
      }

      $redir_path = '/gallery/image/'.$image_id.
	($comment_id ?"#comment-".$comment_id : '');

      $this->redirect(uri_for($redir_path));
    }

    // Delete comment: POST request && comment && image && submit to
    // delete image. Delete!
    if ($comment_id && $image_id &&
	$this->request['params']['delete-comment-submit']) {

      // Comments table
      $query =  sprintf("DELETE FROM comments WHERE id = '%s'",
    		     $this->model->escape($comment_id));
      $this->model->query($query);

      // comments_images table
      $query =  sprintf("DELETE FROM comments_images WHERE comment_id = '%s'",
			$this->model->escape($comment_id));

      $this->model->query($query);

      // comments_users table 
      $query =  sprintf("DELETE FROM comments_users WHERE comment_id = '%s'",
			$this->model->escape($comment_id));

      $this->model->query($query);
      $this->redirect(uri_for ('/gallery/image/'.$image_id));
    }

    $comment_text = $this->request['params']['comment-text'];
    $comment_text = strip_tags($comment_text);

    if (!$comment_text) {
      $this->redirect(uri_for ('/gallery/image/'.$image_id));
    }    

    $error_on_insert = 0;

    $query = sprintf("INSERT INTO comments VALUES(DEFAULT(id),'%s')",
    		     $this->model->escape($comment_text));
    
    $this->model->query($query);  
    $comment_ins_id = $this->model->insert_id();

    if ($comment_ins_id) {
      $query = sprintf("INSERT INTO comments_images VALUES('%s','%s')",
		       $this->model->escape($image_id),
		       $this->model->escape($comment_ins_id));


      $this->model->query($query);
      $comments_images_ins_id = $this->model->insert_id();

      $query = sprintf("INSERT INTO comments_users VALUES('%s','%s')",
		       $this->model->escape($this->userid),
		       $this->model->escape($comment_ins_id));


      $this->model->query($query);
      $comments_users_ins_id = $this->model->insert_id();

      if (!$comments_images_ins_id || !$comments_users_ins_id) {
	$error_on_insert = 1;
      } else {
	$redir_path = '/gallery/image/'.$image_id.
	  ($res_ins['id'] ?"#comment-".$comment_ins_id : '');
      }

    } else {
      $error_on_insert = 1;
    }

    if ($error_on_insert) {
      $this->redirect(uri_for('/gallery/image/'.$image_id));
    } else {
      $this->redirect(uri_for($redir_path));
    }
  }

  // Used to redirect to another action/resourcre/URI
  function redirect($path,$status = '303') {
    header("HTTP/1.1 ".$status);
    header("Location: ".$path);
    exit(0);
  }

  // Checks if user is logged and sets approprieate variables
  private function check_login() {

    if ($this->request['cookies']['photomontage_logged']) {
      if (!$this->model) {
    	$this->db_init();
      }

      $hash = $this->request['cookies']['photomontage_logged'];

      if (!$this->model) {
	$this->stash['db_error'] = 1;
	$this->render();
      } else {
	$query = sprintf("SELECT * FROM logged WHERE ".
			 "hash='%s'",
			 $this->model->escape($hash));

	$res = $this->model->fetch_assoc($this->model->query($query));

	if ($res["user_id"]) {
	  $this->userid = $res["user_id"];

	  // Renew cookie time
 	  setcookie("photomontage_logged",
		    $res['hash'],
		    (time()+3600)); // Expires after one hour
	  

	  $query = sprintf("SELECT users.id,users.username,".
			   "users.real_name,roles.role_name,".
			   "users_roles.role_id ".
			   " FROM users_roles ".
			   " LEFT JOIN users ".
			   " ON users_roles.user_id = users.id ".
			   " LEFT JOIN roles ".
			   " ON users_roles.role_id = roles.id WHERE ".
			   " users.id='%s'",
			   $this->model->escape($this->userid));

	  $res = $this->model->fetch_assoc($this->model->query($query));

	  if ($res["username"]) {
	    $this->user_role = $res['role_name'];
	    $this->stash['user'] = array ('username' => $res['username'],
					  'realname' => $res['real_name'],
					  'role' => $res['role_name']);
	  }
	}
      }
    }
  }

  // Used from functions that habdle patth/action/URIs to require
  // login
  private function force_login() {
    $this->check_login();

    /*  Redirect to /login if the user is not yet authenticated */
    if ($this->userid < 0) {
      $this->redirect(uri_for('/login'));
    }
  }

  // Used from function to intialize the database when needed
  private function db_init () {
    $this->model = new DB();
  }

  // Render the page 
  private function render() {

    if ($this->stash["db_error"]) {
      $this->template = "error";
    }

    define('CONTENT', $this->template.$this->templates['extension']);

    // Extract variables defined for the template context in the
    // current scope.
    extract($this->stash);

    require_once ($this->templates['wrapper']);

    exit(0);
  }
}
?>