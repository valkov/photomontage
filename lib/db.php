<?php
// This file is part of Photomontage -- skill-demonstration PHP web gallery.
// 
// Copyright (C) 2012 Ivaylo Valkov <ivaylo@e-valkov.org>
// 
// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public
// License along with this program.  If not, see <http://www.gnu.org/licenses/>.

?>

<?php
/* Protect the code from execution if not accessed from index.php
   where PHOTOMONTAGE is defined. */
if (!defined('PHOTOMONTAGE')) {
  exit(1);
}

Class DB {

  // Database data
  private $db_name;
  private $db_user;
  private $db_pass;
  private $db_host;
  private $db_link;
  private $db;

  function __construct() {

    // Database
    $this->db_name = DB_NAME;
    $this->db_user = DB_USER;
    $this->db_pass = DB_PASS;
    $this->db_host = DB_HOST;
   
    if (!$this->connect()) {
      return NULL;
    }

    if (!$this->select_db()) {
      return NULL;
    }
  }

  function __destruct() {
    if ($this->db_link) {
      mysql_close($this->db_link);
    }
  }

  private function connect () {
    $db_link = mysql_connect ($this->db_host,
			      $this->db_user,
			      $this->db_pass);

    if (!$db_link) {
      error_log("Photomontage: Error connecting to SQL server ".
		$this->db_host.": ".mysql_error());
      return NULL;
    } else {
      $this->db_link = $db_link;
    }

    return $this->db_link;
  }

  private function select_db () {
    if (!$this->db_link) {
      return NULL;
    }

    $db = mysql_select_db($this->db_name, $this->db_link);


    if (!$db) {
      error_log("Photomontage: Error selecting database ".
		$this->db_name.": ".mysql_error($this->link));
      return NULL;
    }

    $this->db = $db;

    return $this->db;
  }

  function query($query) {

    if (!$this->db_link || !$this->db || !$query) {
      return NULL;
    }

    return mysql_query($query, $this->db_link);
  }

  function fetch_assoc ($result) {
    if (!$this->db_link || !$this->db || !$result) {
      return NULL;
    }

    return mysql_fetch_assoc($result);
  }

  function escape($query) {
    if (!$query) {
      return NULL;
    }

    return mysql_real_escape_string($query);
  }

  function rows_count($result) {
    if (!$this->db_link || !$this->db || !$result) {
      return NULL;
    }

    return mysql_num_rows($result);
  }

  function insert_id() {
    if (!$this->db_link || !$this->db) {
      return NULL;
    }

    return mysql_insert_id($this->db_link);
  }
}
?>